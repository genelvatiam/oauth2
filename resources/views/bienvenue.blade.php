@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Applications cliente </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="row">
                            @foreach($oauthClients as $oauthClient)
                                <button type=button class="btn btn-primary m-1" onclick=window.location.href='http://localhost:8000/oauth/authorize?client_id={{ $oauthClient->id }}&response_type=code&redirect_uri={{ $oauthClient->redirect }}';>{{ $oauthClient->name }}</button>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
