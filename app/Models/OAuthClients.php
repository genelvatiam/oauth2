<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class OAuthClients extends Model
{
    //table
    protected $table = 'oauth_clients';

    //fillable
    protected $fillable = [
        'user_id',
        'name',
        'secret',
        'redirect',
        'personal_access_client',
        'password_client',
        'revoked'
    ];
}
