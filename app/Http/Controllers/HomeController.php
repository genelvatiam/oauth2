<?php

namespace App\Http\Controllers;

use App\Models\OAuthClients;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin', ["only"=>"index"]);

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function index1()
    {
        $oauthclients = DB::table('oauth_clients')->get();
        return view('bienvenue', ['oauthClients' => $oauthclients]);
    }


    public function construct_querry($client_id,$redirect){
        $query = http_build_query([
            'client_id' => $client_id,
            'redirect_uri' => $redirect,
            'response_type' => 'code'
        ]);

        return 'http://your-app.com/oauth/authorize?'.$query;
    }
    public function test()
    {
        $auths = OAuthClients::all();
        //return response()->json(array('response'=>200,'values'=>));
    }
}
