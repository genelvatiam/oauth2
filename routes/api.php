<?php

use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
route::get('/hello',function(){
    return ['bonjour'];
})->middleware(['auth:api','scope:hello']);
route::get('/goodbye',function(){
    return ['aurevoir'];
})->middleware(['auth:api','scope:goodbye']);
route::get('/all',function(){
    return ['bonjour','bonjour'];
})->middleware(['auth:api','scopes:hello,goodbye']);
